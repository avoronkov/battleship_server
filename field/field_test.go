package field

import (
	"battleship_server/cell"
	"testing"
)

const (
	E = cell.EMPTY
	S = cell.SHIP
)

func TestKilled1(t *testing.T) {
	// "X"
	f := Field{}
	f.field[1][1] = cell.KILL
	if !f.killed(1, 1) {
		t.Errorf("Ship at 1,1 is not kiled:\n%s", f)
	}
}

func TestKilled2(t *testing.T) {
	// "X#"
	f := Field{}
	f.field[1][1] = cell.KILL
	f.field[1][2] = cell.SHIP
	if f.killed(1, 1) {
		t.Errorf("Ship at 1,1 is kiled:\n%s", f)
	}
}

func TestKilled3(t *testing.T) {
	// "#X#"
	f := Field{}
	f.field[1][1] = cell.SHIP
	f.field[1][2] = cell.KILL
	f.field[1][3] = cell.SHIP
	if f.killed(2, 1) {
		t.Errorf("Ship at 2,1 is kiled:\n%s", f)
	}
}

func TestKilled4(t *testing.T) {
	// "Xx#"
	f := Field{}
	f.field[1][1] = cell.KILL
	f.field[1][2] = cell.KILL
	f.field[1][3] = cell.SHIP
	if f.killed(1, 1) {
		t.Errorf("Ship at 1,1 is kiled:\n%s", f)
	}
}

func TestKilled5(t *testing.T) {
	// "Xxx#"
	f := Field{}
	f.field[1][1] = cell.KILL
	f.field[1][2] = cell.KILL
	f.field[1][3] = cell.KILL
	f.field[1][4] = cell.SHIP
	if f.killed(1, 1) {
		t.Errorf("Ship at 1,1 is kiled:\n%s", f)
	}
}

func TestKilled6(t *testing.T) {
	// "xXx#"
	f := Field{}
	f.field[1][1] = cell.KILL
	f.field[1][2] = cell.KILL
	f.field[1][3] = cell.KILL
	f.field[1][4] = cell.SHIP
	if f.killed(2, 1) {
		t.Errorf("Ship at 2,1 is kiled:\n%s", f)
	}
}

func TestKilledBorder1(t *testing.T) {
	// "|x"
	f := Field{}
	f.field[0][0] = cell.KILL
	if !f.killed(0, 0) {
		t.Errorf("Ship at 0,0 is not kiled:\n%s", f)
	}
}

func TestKilledBorder2(t *testing.T) {
	// "x|"
	f := Field{}
	f.field[FIELD_SIZE-1][FIELD_SIZE-1] = cell.KILL
	if !f.killed(FIELD_SIZE-1, FIELD_SIZE-1) {
		t.Errorf("Ship at FIELD_SIZE-1,FIELD_SIZE-1 is not kiled:\n%s", f)
	}
}

func TestKilledBorder3(t *testing.T) {
	// "x#|"
	f := Field{}
	f.field[FIELD_SIZE-1][FIELD_SIZE-2] = cell.KILL
	f.field[FIELD_SIZE-1][FIELD_SIZE-1] = cell.SHIP
	if f.killed(FIELD_SIZE-2, FIELD_SIZE-1) {
		t.Errorf("Ship at 8,9 is not kiled:\n%s", f)
	}
}

func TestKilledBorder4(t *testing.T) {
	// "x#|"
	f := Field{}
	f.field[FIELD_SIZE-2][FIELD_SIZE-1] = cell.KILL
	f.field[FIELD_SIZE-1][FIELD_SIZE-1] = cell.SHIP
	if f.killed(FIELD_SIZE-1, FIELD_SIZE-2) {
		t.Errorf("Ship at 9,8 is not kiled:\n%s", f)
	}
}

func TestCheckCorrect(t *testing.T) {
	f := Field{}
	f.field = [FIELD_SIZE][FIELD_SIZE]cell.Cell{
		{S, E, E, E, E, E, E, E, E, E},
		{E, E, S, S, S, S, E, S, E, E},
		{E, E, E, E, E, E, E, S, E, E},
		{E, E, E, E, E, E, E, S, E, E},
		{E, E, S, S, E, S, E, E, E, E},
		{E, E, E, E, E, E, E, E, E, S},
		{E, E, E, E, E, E, E, E, E, E},
		{E, S, S, E, E, E, E, S, S, S},
		{E, E, E, E, S, E, E, E, E, E},
		{E, E, E, E, S, E, S, E, E, E},
	}
	if err := f.Check(); err != nil {
		t.Errorf("Correct field reported as incorrect: %v\n%v", err, f)
	}
}

func TestCheckIntersection1(t *testing.T) {
	f := Field{}
	f.field = [FIELD_SIZE][FIELD_SIZE]cell.Cell{
		{S, E, E, E, E, E, E, E, E, E},
		{E, E, S, S, S, S, E, S, E, E},
		{E, E, E, E, E, E, E, S, E, E},
		{E, E, E, E, S, E, E, S, E, E},
		{E, E, S, S, E, E, E, E, E, E},
		{E, E, E, E, E, E, E, E, E, S},
		{E, E, E, E, E, E, E, E, E, E},
		{E, S, S, E, E, E, E, S, S, S},
		{E, E, E, E, S, E, E, E, E, E},
		{E, E, E, E, S, E, S, E, E, E},
	}
	err := f.Check()
	if err == nil {
		t.Errorf("interseption is not reported: \n%v", f)
	} else if msg := err.Error(); msg != "Intersecting ships at (4, 3), (3, 4)" {
		t.Errorf("incorrect message: %v", msg)
	}
}

func TestCheckIntersection2(t *testing.T) {
	f := Field{}
	f.field = [FIELD_SIZE][FIELD_SIZE]cell.Cell{
		{S, E, E, E, E, E, E, E, E, E},
		{E, E, S, S, S, S, E, S, E, E},
		{E, E, E, E, E, E, E, S, E, E},
		{E, E, E, E, E, E, E, S, E, E},
		{E, E, S, S, E, E, E, E, E, E},
		{E, E, E, E, S, E, E, E, E, S},
		{E, E, E, E, E, E, E, E, E, E},
		{E, S, S, E, E, E, E, S, S, S},
		{E, E, E, E, S, E, E, E, E, E},
		{E, E, E, E, S, E, S, E, E, E},
	}
	err := f.Check()
	if err == nil {
		t.Errorf("Interseption is not reported: \n%v", f)
	} else if msg := err.Error(); msg != "Intersecting ships at (3, 4), (4, 5)" {
		t.Errorf("Incorrect message: %v", msg)
	}
}

func TestCheckShipTooLong1(t *testing.T) {
	f := Field{}
	f.field = [FIELD_SIZE][FIELD_SIZE]cell.Cell{
		{E, E, E, E, E, E, E, E, E, E},
		{E, S, S, S, S, S, E, S, E, E},
		{E, E, E, E, E, E, E, S, E, E},
		{S, E, E, E, E, E, E, S, E, E},
		{E, E, S, S, E, S, E, E, E, E},
		{E, E, E, E, E, E, E, E, E, S},
		{E, E, E, E, E, E, E, E, E, E},
		{E, S, S, E, E, E, E, S, S, S},
		{E, E, E, E, S, E, E, E, E, E},
		{E, E, E, E, S, E, S, E, E, E},
	}
	err := f.Check()
	if err == nil {
		t.Errorf("Long ship is not reported: \n%v", f)
	} else if msg := err.Error(); msg != "Ship is too long (--) at (5, 1)" {
		t.Errorf("Incorrect message: %v", msg)
	}
}

func TestCheckShipTooLong2(t *testing.T) {
	f := Field{}
	f.field = [FIELD_SIZE][FIELD_SIZE]cell.Cell{
		{E, E, E, E, E, E, E, E, E, E},
		{E, S, S, S, E, E, E, S, E, E},
		{E, E, E, E, E, E, E, S, E, E},
		{S, E, E, E, E, E, E, S, E, E},
		{E, E, S, S, E, S, E, S, E, E},
		{E, E, E, E, E, E, E, S, E, S},
		{E, E, E, E, E, E, E, E, E, E},
		{E, S, S, E, E, E, E, S, S, S},
		{E, E, E, E, S, E, E, E, E, E},
		{E, E, E, E, S, E, S, E, E, E},
	}
	err := f.Check()
	if err == nil {
		t.Errorf("Long ship is not reported: \n%v", f)
	} else if msg := err.Error(); msg != "Ship is too long (|) at (7, 5)" {
		t.Errorf("Incorrect message: %v", msg)
	}
}

func TestCheckNotManyOneSizedShips(t *testing.T) {
	f := Field{}
	f.field = [FIELD_SIZE][FIELD_SIZE]cell.Cell{
		{E, E, E, E, E, E, E, E, E, E},
		{E, E, S, S, S, S, E, S, E, E},
		{E, E, E, E, E, E, E, S, E, E},
		{E, E, E, E, E, E, E, S, E, E},
		{E, E, S, S, E, S, E, E, E, E},
		{E, E, E, E, E, E, E, E, E, S},
		{E, E, E, E, E, E, E, E, E, E},
		{E, S, S, E, E, E, E, S, S, S},
		{E, E, E, E, S, E, E, E, E, E},
		{E, E, E, E, S, E, S, E, E, E},
	}
	err := f.Check()
	if err == nil {
		t.Errorf("Not enough 1-sized ships is not reported: \n%v", f)
	} else if msg := err.Error(); msg != "Incorrect number of ships of size 1: expected 4, found 3" {
		t.Errorf("Incorrect message: %v", msg)
	}
}

func TestCheckTooMany2SizedShips(t *testing.T) {
	f := Field{}
	f.field = [FIELD_SIZE][FIELD_SIZE]cell.Cell{
		{S, E, E, E, E, E, E, E, E, E},
		{E, E, S, S, S, S, E, S, E, E},
		{E, E, E, E, E, E, E, S, E, E},
		{S, E, E, E, E, E, E, S, E, E},
		{S, E, S, S, E, S, E, E, E, E},
		{E, E, E, E, E, E, E, E, E, S},
		{E, E, E, E, E, E, E, E, E, E},
		{E, S, S, E, E, E, E, S, S, S},
		{E, E, E, E, S, E, E, E, E, E},
		{E, E, E, E, S, E, S, E, E, E},
	}
	err := f.Check()
	if err == nil {
		t.Errorf("Too many 2-sized ships is not reported: \n%v", f)
	} else if msg := err.Error(); msg != "Incorrect number of ships of size 2: expected 3, found 4" {
		t.Errorf("Incorrect message: %v", msg)
	}
}

func TestCheckNotMany4SizedShips(t *testing.T) {
	f := Field{}
	f.field = [FIELD_SIZE][FIELD_SIZE]cell.Cell{
		{S, E, E, E, E, E, E, E, E, E},
		{E, E, E, E, E, E, E, S, E, E},
		{E, E, E, E, E, E, E, S, E, E},
		{E, E, E, E, E, E, E, S, E, E},
		{E, E, S, S, E, S, E, E, E, E},
		{E, E, E, E, E, E, E, E, E, S},
		{E, E, E, E, E, E, E, E, E, E},
		{E, S, S, E, E, E, E, S, S, S},
		{E, E, E, E, S, E, E, E, E, E},
		{E, E, E, E, S, E, S, E, E, E},
	}
	err := f.Check()
	if err == nil {
		t.Errorf("Not enough 4-sized ships is not reported: \n%v", f)
	} else if msg := err.Error(); msg != "Incorrect number of ships of size 4: expected 1, found 0" {
		t.Errorf("Incorrect message: %v", msg)
	}
}

func TestCheckIncorrectCell(t *testing.T) {
	f := Field{}
	f.field = [FIELD_SIZE][FIELD_SIZE]cell.Cell{
		{S, E, E, E, E, E, E, E, E, E},
		{E, E, S, S, S, S, E, S, E, E},
		{E, E, E, E, E, E, E, S, E, E},
		{E, E, E, E, E, S, E, S, E, cell.MISS},
		{E, E, S, S, E, E, E, E, E, E},
		{E, E, E, E, E, E, E, E, E, S},
		{E, E, E, E, E, E, E, E, E, E},
		{E, S, S, E, E, E, E, S, S, S},
		{E, E, E, E, S, E, E, E, E, E},
		{E, E, E, E, S, E, S, E, E, E},
	}
	err := f.Check()
	if err == nil {
		t.Errorf("incorrect cell is not reported: \n%v", f)
	} else if msg := err.Error(); msg != "Incorrect cell in (9, 3): -" {
		t.Errorf("incorrect message: %v", msg)
	}
}
