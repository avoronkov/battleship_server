#!/usr/bin/env python3

import random
import sys

def arrange():
    ships = (
        "1101001111",
        "0001000000",
        "0001000001",
        "0000000001",
        "0100000000",
        "0100001000",
        "0100000000",
        "0000000000",
        "0001010010",
        "1000000010",
    )
    print("\n".join(ships))
    sys.stdout.flush()
    print("<P1> arranged", file=sys.stderr)

class Shooter:
    def __init__(self):
        self._shoots = [(x, y) for y in range(10) for x in [chr(ord('A') + i) for i in range(10)]] 
        random.shuffle(self._shoots)

    def shoot(self):
        (x, y) = self._shoots.pop()
        print("{} {}".format(x, y))
        sys.stdout.flush()
        print("<P1> shot {} {} (left {})".format(x, y, len(self._shoots)), file=sys.stderr)

shooter = Shooter()

for line in sys.stdin:
    cmd = line.strip()
    print("<P1> cmd = {}".format(cmd), file=sys.stderr)
    if cmd == "Arrange!":
        arrange()
    elif cmd in ("Shoot!", "Hit", "Kill"):
        shooter.shoot()
    elif cmd in ("Win!", "Lose"):
        print("<P1> finished: %v".format(cmd))
        break
    else:
        print("<P1> Ignoring: {}".format(cmd), file=sys.stderr)

print("<P1> Stdin closed.", file=sys.stderr)
