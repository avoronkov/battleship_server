# Battleship server.

## How-to

- Install go compiler.

- (Set up `$GOPATH`)

- `cd battleship_server`

- `go build`

- Run server with 2 clients: `./battleship_server <prog1> <prog2>`
